//  ___FILEHEADER___

import UIKit
import RxSwift
import Cartography

class ___FILEBASENAMEASIDENTIFIER___: BaseViewController {
    
   private var presenter: ___VARIABLE_productName:identifier___PresenterProtocol {
       return basePresenter as! ___VARIABLE_productName:identifier___PresenterProtocol
   }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
