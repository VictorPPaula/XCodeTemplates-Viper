//  ___FILEHEADER___

import RxSwift
import RxCocoa

protocol ___FILEBASENAMEASIDENTIFIER___Protocol: class {
    
}

class ___FILEBASENAMEASIDENTIFIER___: BaseInteractor {
 
    private let repository: ___VARIABLE_productName:identifier___RepositoryProtocol
    
    init(repository: ___VARIABLE_productName:identifier___RepositoryProtocol) {
        self.repository = repository
        super.init()
    }
}

extension ___FILEBASENAMEASIDENTIFIER___: ___FILEBASENAMEASIDENTIFIER___Protocol {
    
}
